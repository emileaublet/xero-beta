importScripts("../javascripts/jimp.min.js");

function callback(e) {
	console.log(e);
}


self.addEventListener("message", function(e) {
	images = []
    Jimp.read(e.data[0]).then(function (work) {
    	for(var r = 0; r < e.data[1].length; r++){
	        work.resize(e.data[1][r].resize[0], e.data[1][r].resize[1])
	             .quality(87) // Todo: Find perfect Quality (87 seems to be good but need more tests with colours & patterns)
	             .getBase64(Jimp.AUTO, function (err, src) {
	             	images.push([src, e.data[3], e.data[1][r].resize[0], e.data[1][r].resize[1], e.data[4]]);
	             });
	    };
	    self.postMessage(images);
    });
    console.log()
});