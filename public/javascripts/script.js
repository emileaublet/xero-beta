/**
 *
 * XERO 
 * Aldo's Image processor
 *
 * Author: Émile Aublet
 * Email: eaublet@aldogroup.com
 *
 */
/**

	TODO:
	- Ignore LocalHost for GA
	- Clean code

 */


/*=================================
=            Variables            =
=================================*/

var c  = document.createElement('canvas');
var cx = c.getContext('2d');
var thumbwidth = 100;
var thumbheight = 100;
var crop = false;
var background = 'white';
var jpeg = false;
var quality = 0.8;
var processing = false;
var filesToProcess = 1;
var filesToProcessTotal = 0;
var recipes = new Array;
var recipesLoaded = false;
var wildcard = '*';
var workers = 4;
var modalOpen = false;
var imgCount = 1;
var to = 0;
var processStarted = false;
var queue = [];
var uploadedFiles = 0;
// var queue=new Queue;
var startClock = 0;
var zip = new JSZip();



/*========================================
=            Google Analytics            =
========================================*/

/*----------  Init GA  ----------*/

function gaInit(){
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-90070405-2', 'auto');
	ga('send', 'pageview', location.href + 'xerobeta');

}
// /*----------  Log GA Events  ----------*/

function gaLog (msg, type){
	switch(type) {
	    case "success":
	        ga('send', 'event', 'Xero Beta', 'Success (SOURCES | VERSIONS | TIME TAKEN)', msg);
	        break;
	    case "error":
	        ga('send', 'event', 'Xero Beta', 'Error', msg);
	        console.log('...');
	        break;
	    default:
	        ga('send', 'event', 'Xero Beta', 'Event', msg);
	}
}
/*=====  End of Google Analytics  ======*/



/*===============================
=            Helpers            =
===============================*/

/*----------  Can Split  ----------*/

var canSplit = function(str, token){
    return (str || '').split(token).length > 1;         
}


/*----------  Log an Error  ----------*/

logError = function(errorname, filename){
	newError = '<li><b>' + filename + ' cannot be processed.</b><br/>' + errorname + '</li>';
	$('span.error ul').append(newError);
	gaLog(filename, 'error');
}

/*----------  Zip a file  ----------*/

function zipIt () {
	zip.generateAsync({type:"blob"}).then(function(content) {
		saveAs(content, "images" + Date.now() + '.zip')
	})
}

function addToZip (b64, filename, width, height, path) {
	var img = document.createElement("img");
	imgName = filename + '_' + width + 'x' + height + '.jpg'
	imgData = b64;

	img.setAttribute("src", imgData);
	img.setAttribute("filename", imgName);

	if(path.length){
		var folder = path.split('/')
		folder.pop()
		if(folder[0] == 'ecom'){
			ecomFolder = imgName.split('_')
			zip.folder('ecom/' + ecomFolder[0] + '/' + ecomFolder[2]).file(imgName, imgData.split(',')[1], {base64: true});
		}
		else{
			zip.folder(folder.join('/')).file(imgName, imgData.split(',')[1], {base64: true});
		}
	}
	else{
		zip.file(imgName, imgData.split(',')[1], {base64: true});
	}
}

/*----------  Encode in b64  ----------*/

function Base64EncodeUrl(str){
    return str.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, '');
}

/*----------  Decode b64  ----------*/

function Base64DecodeUrl(str){
    str = (str + '===').slice(0, str.length + (str.length % 4));
    return str.replace(/-/g, '+').replace(/_/g, '/');
}

/*----------  b64 to Array Buffer  ----------*/

function _base64ToArrayBuffer(base64) {
    var binary_string =  window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array( len );
    for (var i = 0; i < len; i++)        {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

/*----------  Show Modal if Folder is not supported  ----------*/

function noSupportFolder () {

	if(modalOpen == false){
		$('.nosupportfolder').addClass('active');
		modalOpen = true;
	}
	$('.nosupportfolder .close').click(function(){
		$('.nosupportfolder').removeClass('active');
		modalOpen = false;
	})
}

/*=====  End of Section Helpers  ======*/







/*==========================
=            UI            =
==========================*/

/*----------  Update Count  ----------*/

function startProcess () {
	if(processStarted == false){
		$('.process').show();
		startClock = Date.now();
		processStarted = true;
	}
}

function endProcess (date) {
	
	$('span.timer').html(((date - startClock)/1000).toFixed(2) + "sec");
	$('.process').hide();
	$('.drophere').addClass('undropable');

	$('.drophere .section.loading').hide();
	var errorsList = $('.drophere .section.loading .error');

	$('.drophere .section.ready').show().append(errorsList);
	$('.drophere').addClass('download');
		
	sendGA = {
		sources: uploadedFiles,
		versions: filesToProcessTotal,
		timetaken: ((Date.now() - startClock)/1000).toFixed(2) + "sec"
	}

	gaLog(JSON.stringify(sendGA), 'success');

	$('.drophere').click(function() {
		zipIt();
		gaLog('Zip downloaded', 'event');
	})
}

function addToCount (q) {
	// filesToProcess++;
	// filesToProcessTotal++q;
	currentTotal = filesToProcessTotal;
	filesToProcessTotal = currentTotal + q;
	// $('span.done').html(0);
	$('span.total').html(filesToProcessTotal);
}

/*----------  Decrease Count  ----------*/

/**

	TODO:
	- Replace this function

 */


function decreaseCount() {
	$('span.done').html(filesToProcess++);
	if(filesToProcess == filesToProcessTotal){
		endProcess(Date.now());
	}
}


function updateFileToProcess (e) {
	if(e){
		if(filesToProcess == 0){
			$('.process').show();
			startClock = Date.now();
		}
		filesToProcess++;
		filesToProcessTotal++;
		$('span.done').html(filesToProcessTotal - filesToProcess);
		$('span.total').html(filesToProcessTotal);
	}
	else{
		filesToProcess--;
		if(filesToProcess == 0){
			processing = true; 
			$('.process').hide();
			$('.drophere').addClass('undropable');

			$('.drophere .section.loading').hide();
			var errorsList = $('.drophere .section.loading .error');

			$('.drophere .section.ready').show().append(errorsList);
			$('.drophere').addClass('download');
			
			gaLog(filesToProcessTotal, 'success');

			$('.drophere').click(function() {
				zipIt();
				gaLog('Zip downloaded', 'event');
			})
		}
		else{
			$('span.done').html(filesToProcessTotal - filesToProcess);
			$('span.timer').html(((Date.now() - startClock)/1000).toFixed(2) + "sec");
		}
	}
}







/*=====================================
=            Assign Recipe            =
=====================================*/

/*----------  Load Recipes  ----------*/
/**

	TODO:
	- Put recipes on Firebase

 */


function loadRecipes (callback){
	var getRecipes = $.getJSON( "../javascripts/recipes.json", function( data ) {
	  $.each( data, function( key, val ) {
		  recipes.push( val );
		});
	});
	getRecipes.complete(function(){
		init();
	});
};

/*----------  Assign Matching Recipe  ----------*/

function checkRecipes(params, fullname){
	matchingRecipe = [];
	for(var i = 0; i < recipes.length; i++){
		for(var c = 0; c < recipes[i].conditions.length; c++){
			recipe = JSON.stringify(recipes[i].conditions[c].condition).replace(/['"]+/g, '').split(' ');
			if(recipe.length == params.length){
				if(compareRecipeAndParams(recipe, params, fullname)){
					matchingRecipe.push(recipes[i]);
				}
			}
		}
	}
	if(matchingRecipe.length > 0){
		return matchingRecipe;

	}
	else{
		/**
		
			TODO:
			- Return Error # instead of string
		
		 */
		uploadedFiles--;
		logError('This file has no matching recipes. Make sure the file name and size are correct.', fullname);
		return false
	}
}

/*----------  Compare All Matching recipes to find the one (including Widlcards)  ----------*/

function compareRecipeAndParams(recipe, params, fullname){
	for(var p = 0; p < recipe.length; p++){
		if(compare(recipe[p], params[p]) == false){
			return false;
		}
	}
	return true
}
function compare (a, b){
	if(a == wildcard){
		return true
	}
	else if(a == b){
		return true
	}
	else{
		return false
	}
}


/*=====  End of Assign Recipe  ======*/



/*==================================
=            Crop Files            =
==================================*/

/*----------  Crop File  ----------*/

function queueThis (b64, fileName, recipe, fullpath) {

	resizeImage(b64.result, recipe[0].crops, 100, fileName, fullpath);
	// queue.push({img: b64.result, crops: recipe[0].crops, q: 100, filename: fileName, path: fullpath});
	// if(uploadedFiles == queue.length){
	// 	processQueue();
	// }
}

/*----------  Crop File  ----------*/

function cropThis (b64, fileName, recipe, fullpath) {
	resizeImage(b64.result, recipe[0].crops, 100, fileName, fullpath);
}



/*----------  Receive Image  ----------*/

// function receiveImage(data) {
// 		queue.add(decreaseCount);
// 		return

// }

function processQueue() {
	if(queue.length > 0){
		item = queue[0];
		// resizeImage(item.img, item.crops, item.q, item.filename, item.path);
		queue.splice(0, 1);
		processQueue();
	}
	else{
		console.log('Queue is finished');
	}
}

/*----------  Resize image  ----------*/

function resizeImage (b64, crops, q, filename, fullpath) {
	for(var c = 0; c < crops.length; c++){
		// updateFileToProcess(true);
	};
	// var workId = crew.addWork([b64, crops, q, filename, fullpath]);

	// Resizer...
	param = {
	  _image: b64,
	  _crops: crops,
	  _q: q,
	  _filename: filename,
	  _fullpath: fullpath
	};
	$.ajax({
	  type: 'POST',
	  url: '../resize/' + Date.now(),
	  data: param,
	  success: function(data){
	  	data.forEach(function(imgReturned){
	  		decreaseCount();
	  		addToZip (imgReturned._output, imgReturned._name, imgReturned._width, imgReturned._height, imgReturned._path);
	  	});
	  },
	  // dataType: dataType,
	  async:false
	});
	// $.post('../resize', param, function(data) {
		
	// 	// queue.push(data);
	// 	// console.log(queue.length);
	//   // console.log("Received a message: " + data);
	//   	data.forEach(function(imgReturned)
	//   		{
	//   			receiveImage(imgReturned);
	//   	// 		queue.push(imgReturned);
	// 			// console.log(queue.length);
	//   	// 	// console.log(imgReturned);
	//   		});
	// 	// console.log(crop);
	// 		// console.log(imgReturned[1]);
	// 		// console.log(imgReturned);
	// 		// console.log('got one image');
	// 		// 
	// 	// });
	//   return 
	// });
}

/*=====  End of Crop File  ======*/





/*=====================================
=            Receive Files            =
=====================================*/

/*----------  Add Dropzone to DIV  ----------*/

function dragHere (drop) {
	var drophere = new Dropzone(drop, {
		url: '/',
		clickable: false,
		addedfile: function(file) {
			$(drop).find('.section.default').hide();
			$(drop).find('.section.loading').show();
			getProperties(file);
			uploadedFiles++;
		}
	});
	drophere.on('dragover', function(file){
		$(drop).addClass('dragging');
	});
	drophere.on('dragleave', function(file){
		$(drop).removeClass('dragging');
	});
	drophere.on('drop', function(file){
		$(drop).removeClass('dragging');
	});

	drophere.uploadFiles = function(files) {
	    var self = this;
	    self.emit("success", file, 'success', null);
	    self.emit("complete", file);
	    self.processQueue();
	}
}

/*----------  Validate File & Check for Recipe  ----------*/

function getProperties (files) {
		file = files;
		// Currently Accepting only Jpeg
		var imageType = /^image\/jpeg/;
		
		if (!imageType.test(file.type)) {
		  logError('Currently, only JPEG can be processed', file.name);
		  return false;
		}

		var fullname = file.name; // will replace this by file.name
		var name = fullname.replace(/\.[^/.]+$/, "");
		var fullpath = '';
		if(file.fullPath){
			fullpath = file.fullPath;
		}
		var params = name.split('_');
		recipeResult = checkRecipes(params, fullname);
		
		if(recipeResult == false){
			return
		}
		
		else{
			cropQty = recipeResult[0].crops.length;
			addToCount(cropQty);
			var img = document.createElement("img");
			img.file = file;
			var reader = new FileReader();
			reader.onload = function(img) {
				recipeResult = checkRecipes(params, fullname);
				if(recipeResult == false){
					return
				}
				else{
					if(processStarted == false){
						startProcess();
					}
					params.pop();
					cropThis(this,  params.join('_'), recipeResult, fullpath);
				}

			};
			reader.readAsDataURL(file);
		}

		

}





/*============================
=            Init            =
============================*/

function init () {	
	gaInit();
	
	dragHere('.drophere');

	if(!/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())){
	 noSupportFolder();
	}

	$( "#fileUpload" ).change( function() {
		$.each($(this).get(0).files, function(){
			handleFiles(this);
		});
	});
	$( "#testConfig" ).bind('change', function() { 
		var newValue = $(this).val();
		checkConfig(newValue);
	});
}

$( document ).ready(function() {
	loadRecipes();
});

/*=====  End of Init  ======*/