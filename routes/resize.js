var express = require('express');
var router = express.Router();
var sharp = require('sharp');
var imagemin = require('imagemin');
var imageminJpegtran = require('imagemin-jpegtran');
var imageminJpegoptim = require('imagemin-jpegoptim');
var imageminJpegRecompress = require('imagemin-jpeg-recompress');


// var fs = require('fs');
// var http = require('http');


// http.createServer(function (req, res) {
//   var outputStream = res;
//   inputStream.pipe(transformer).pipe(outputStream);
// });
// router.all('/', function(req, res){
// 	var image = new Buffer(req.body._image, 'base64');
// 	var inputStream = image;
// 	var transformer = sharp().resize(100);
// 	var outputStream = res;
//   	inputStream.pipe(transformer).pipe(outputStream);
// });

/* GET home page. */
router.post('/:date', function(req, res, next) {
	// fn = req.body._filename;
	

		var crops = req.body._crops;
		filename = req.body._filename;
		fullpath = req.body._fullpath;
 
		image = req.body._image.split(',');
		buf = Buffer.from(image[1], 'base64'); // Ta-d
 
		res.setHeader('Content-Type', 'application/json');

		var response = [];

		crops.forEach(function(crop) {
 
			width = parseInt(crop.resize[0]);
			height = parseInt(crop.resize[1]);

			sharp(buf)
				.resize(width, height)
				// .jpeg({quality: 73})
				.toBuffer(function (err, buffer, info){
					if(crop.resize[0] < 1400){
						max = 100
					}
					else{
						max = 70
					}
					imagemin.buffer(buffer, {
					    // plugins: [imageminJpegoptim({max: 70, progressive: true})]
					    use: [
							imageminJpegRecompress({strip:true, min: 40, max: max, progressive: true, subsample: false})
						]
					}).then(function (files) {
					    item = {
							_name: filename,
							_width: crop.resize[0],
							_height: crop.resize[1],
							_path: fullpath,
							_output: image[0] + ',' + files.toString('base64')
						};
						response.push(item);
						if(response.length == crops.length){
							res.write(JSON.stringify(response));
							res.end();
						};
					});
					
				});
		});
	// });

	// function checkStatus () {
	// 	if (response.length == crops.length) {
	// 		// console.log(JSON.stringify(response));
	// 		res.write(JSON.stringify(response));
	// 		res.end();
	// 	}
	// };
	// console.log(response);
	// res.write(JSON.stringify(response));
	
	// 	var width = crop.resize[0];
	// 	var height = crop.resize[1];
	// 	// res.stream(width + ',' + height);
	// 	sharp(buf).resize(400)
	// 	// 	.resize(crop.resize[0], crop.resize[1])
	// 		// .jpeg({quality: 87, chromaSubsampling: '4:4:4'})
	// 		// .toFile('testOutput/' + filename + '_' + width + 'x' +'.jpg')
	// 		.jpeg()
	// 		.toBuffer(function (err, buffer, info){
	// 			// files.push('yo');
	// 			// console.log(info);
	// 			files.push(info);
	// 			// var outputImg = image[0] + ',' + buffer.toString('base64');
	// 			// files.push([outputImg, filename, crop.resize[0], crop.resize[1], fullpath]);
	// 		});
	// 	// sharp(buf)
	// 	//   .rotate()
	// 	//   .resize(200)
	// 	//   .toBuffer(function (err, buffer, info){
	// 	//   	res.send(buffer);
	// 	//   });
	// 	//   // .then( data => console.log('sent') )
	// 	//   // .catch( err => console.log(err) );
	// });
	// console.log(files);
	// res.send(files);
	// sharp.queue.on('change', function(pos) {
	// 	// console.log(files);
	// 	// res.send(files);
	// 	var sharpQueue = sharp.counters(); 
	// 	if(sharpQueue.queue === 0){
	// 		console.log('complete');
	// 		res.send(files);
	// 		return
	// 	}
	// });
	
	// console.time('Resizing');
	// sharp.queue.on('change', function(pos) {
	//   console.log(sharp.counters()); 
	//   if(sharp.counters() == 0){
	//     // console.timeEnd('Resizing');  
	//     // res.write(response);
	//     console.log(pos);
	//     // res.end();  
	//   }
	//   // console.timeEnd('Resizing');

	  
	//   // console.log(sharp.concurrency());
	//   return;

	// });
});

// app.all('/resize', function(req, res) {
//   // var filename = req.query._filename;
//   // var image = req.query._image
//   // console.log(req); 
//   console.log(req.body._image);
//   res.send(req.body._filename);
// });

module.exports = router;
